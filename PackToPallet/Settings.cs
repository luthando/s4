﻿using System;
using System.IO;
using System.Windows.Forms;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Navigation;
using System.Drawing;
using DevExpress.Utils.Gesture;
using DevExpress.Utils.TouchHelpers;
using DevExpress.Utils.Animation;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Taskbar.Core;
using DevExpress.Utils.Taskbar;
using DevExpress.SovFoods.Modules;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Data;
using DevExpress.XtraGrid.Views.Tile;
using DevExpress.XtraGrid;
using DevExpress.Data.Filtering;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Configuration;


namespace DevExpress.SovFoods {
    public partial class Settings : XtraForm {
        public Assembly myModuleAssembly;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public Settings()
        {
            TaskbarHelper.InitDemoJumpList(TaskbarAssistant.Default, this);
            ShowSplashScreen();
            InitializeComponent();
            DevExpress.Utils.About.UAlgo.Default.DoEventObject(DevExpress.Utils.About.UAlgo.kDemo, DevExpress.Utils.About.UAlgo.pWinForms, this);
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseDefaultWaitForm();
        }

        void ShowSplashScreen() {
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowDefaultWaitForm();
        }

        //resize controls based on screen size
        public void responsiveTiles()
        {

            if ((this.Width <= 820 || this.Height <= 620))
            {
                this.label3.Location = new System.Drawing.Point(427, 59);
                this.label6.Location = new System.Drawing.Point(690, 59);
                this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
                this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
                this.label6.Font = new System.Drawing.Font("Segoe UI", 12F);
                this.label6.Size = new System.Drawing.Size(118, 30);
            }
            else if ((this.Width < 1300 || this.Height < 820))
            {
                this.label3.Location = new System.Drawing.Point(727, 59);
                this.label6.Location = new System.Drawing.Point(990, 59);
                this.label1.Font = new System.Drawing.Font("Segoe UI", 16F);
                this.label3.Font = new System.Drawing.Font("Segoe UI", 16F);
                this.label6.Font = new System.Drawing.Font("Segoe UI", 16F);
                this.label6.Size = new System.Drawing.Size(138, 30);
            }
            else
            {
                this.label3.Location = new System.Drawing.Point(927, 69);
                this.label6.Location = new System.Drawing.Point(1202, 69);
                this.label1.Font = new System.Drawing.Font("Segoe UI", 16F);
                this.label3.Font = new System.Drawing.Font("Segoe UI", 16F);
                this.label6.Font = new System.Drawing.Font("Segoe UI", 16F);
                this.label6.Size = new System.Drawing.Size(138, 30);
            }
        }
  
        void Settings_Load(object sender, EventArgs e)
        {
            //controls responsiveness
            responsiveTiles();

            //check application connection
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (config.AppSettings.Settings["IPAddress"] != null)
            {
                txtIpAddress.Text = config.AppSettings.Settings["IPAddress"].Value;
                txtDbName.Text = config.AppSettings.Settings["DatabaseName"].Value;
                try
                {
                    using (var conn = new SqlConnection("Data Source=tcp:" + config.AppSettings.Settings["IPAddress"].Value + ";Initial Catalog=" + txtDbName.Text + ";Integrated Security=False;User Id=" + config.AppSettings.Settings["Username"].Value + ";Password=" + config.AppSettings.Settings["Password"].Value + ";"))
                    {
                        conn.Open();

                        this.label6.BackColor = System.Drawing.Color.Green;
                        this.label6.Text = "Connected";
                    }
                }
                catch (SqlException)
                {
                }
            }

            if (config.AppSettings.Settings["ReportsServer"] != null)
            {
                txtReportServer.Text = config.AppSettings.Settings["ReportsServer"].Value.ToString();
                txtVirtualDirectory.Text =  config.AppSettings.Settings["VirtualDirectory"].Value.ToString();
                txtPath.Text = config.AppSettings.Settings["Path"].Value.ToString();
            }
            
        }
        void navButtonClose_ElementClick(object sender, NavElementEventArgs e) {
            dispatcherTimer.Stop();
            Application.Exit();
        }

        void navButtonHelp_ElementClick(object sender, NavElementEventArgs e) {
        }

        //when the application window is resize, resize controls accordingly
        private void Settings_Resize(object sender, EventArgs e)
        {
            responsiveTiles();
        }

        //Test database connection
        private void btnConnect_Click(object sender, EventArgs e)
        {
            this.label6.BackColor = System.Drawing.Color.Gold;
            this.label6.Text = "Connecting...";
            bool networkUp = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            if (networkUp)
            {
                
                this.label6.ForeColor = System.Drawing.Color.White;

                try
                {
                    using (var conn = new SqlConnection("Data Source=tcp:" + txtIpAddress.Text + ";Initial Catalog=" + txtDbName.Text + ";Integrated Security=False;User Id=" + txtUserName.Text + ";Password=" + txtPassword.Text + ";"))
                    {
                        conn.Open();

                        this.label6.BackColor = System.Drawing.Color.Green;
                        this.label6.Text = "Connected";
                        var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
                        connectionStringsSection.ConnectionStrings[2].ConnectionString = "metadata=res://*/Modules.PackToPallete.csdl|res://*/Modules.PackToPallete.ssdl|res://*/Modules.PackToPallete.msl;provider=System.Data.SqlClient;provider connection string=\"data source=tcp:" + txtIpAddress.Text + ";initial catalog=" + txtDbName.Text + ";integrated security=False;User Id=" + txtUserName.Text + ";Password=" + txtPassword.Text + ";MultipleActiveResultSets=True;App=EntityFramework\"";
                        connectionStringsSection.ConnectionStrings[1].ConnectionString = "Data Source=tcp:" + txtIpAddress.Text + ";Initial Catalog=" + txtDbName.Text + ";Integrated Security=False;User Id=" + txtUserName.Text + ";Password=" + txtPassword.Text + ";";
                        config.Save();
                        ConfigurationManager.RefreshSection("connectionStrings");
                        MessageBox.Show("Successfully connected!.");

                        
                        //Save database connection to the config file
                        if (config.AppSettings.Settings["IPAddress"] == null)
                        {
                            config.AppSettings.Settings.Add("IPAddress", txtIpAddress.Text.ToString());
                            config.AppSettings.Settings.Add("DatabaseName", txtDbName.Text.ToString());
                            config.AppSettings.Settings.Add("Username", txtUserName.Text.ToString());
                            config.AppSettings.Settings.Add("Password", txtPassword.Text.ToString());
                        }
                        else
                        {
                            config.AppSettings.Settings["IPAddress"].Value = txtIpAddress.Text.ToString();
                            config.AppSettings.Settings["DatabaseName"].Value = txtDbName.Text.ToString();
                            config.AppSettings.Settings["Username"].Value = txtUserName.Text.ToString();
                            config.AppSettings.Settings["Password"].Value = txtPassword.Text.ToString();
                        }
                        config.Save(ConfigurationSaveMode.Modified);
                        ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
                    }
                }
                catch (SqlException)
                {
                    MessageBox.Show("Could not connect to the database with the entered details.");
                    this.label6.BackColor = System.Drawing.Color.Red;
                    this.label6.Text = "Disconnected";
                }
            }
            else
            {
                MessageBox.Show("Network Problem: Could not connect to the database. Please check your network connection.");
                this.label6.BackColor = System.Drawing.Color.Red;
                this.label6.Text = "Disconnected";
            }

          
        }

        //Save connection settings to the config file
        private void btnSave_Click(object sender, EventArgs e)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (txtReportServer.Text == "" || txtVirtualDirectory.Text == "" || txtPath.Text == "")
            {
                MessageBox.Show("Please fill in the missing Reports details.");
            }
            else
            {
                if (config.AppSettings.Settings["ReportsServer"] == null)
                {
                    config.AppSettings.Settings.Add("ReportsServer", txtReportServer.Text.ToString());
                    config.AppSettings.Settings.Add("VirtualDirectory", txtVirtualDirectory.Text.ToString());
                    config.AppSettings.Settings.Add("Path", txtPath.Text.ToString());
                }
                else
                {
                    config.AppSettings.Settings["ReportsServer"].Value = txtReportServer.Text.ToString();
                    config.AppSettings.Settings["VirtualDirectory"].Value = txtVirtualDirectory.Text.ToString();
                    config.AppSettings.Settings["Path"].Value = txtPath.Text.ToString();
                }
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);


                this.Hide();
                Form st = new Settings();
                Form main = new MainForm();

                main.Show();
                st.Close();
            }
        }

    }
}
