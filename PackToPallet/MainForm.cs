﻿using System;
using System.IO;
using System.Windows.Forms;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Navigation;
using System.Drawing;
using DevExpress.Utils.Gesture;
using DevExpress.Utils.TouchHelpers;
using DevExpress.Utils.Animation;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Taskbar.Core;
using DevExpress.Utils.Taskbar;
using DevExpress.SovFoods.Modules;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using DevExpress.XtraPrintingLinks;
using System.Configuration;
using System.Data.Entity.SqlServer;
using System.Data.Objects;
using DevExpress.XtraGrid.Views.Grid;


namespace DevExpress.SovFoods {
    public partial class MainForm : XtraForm {
        int tableFilter = 0;
        int ipcFilter = 0;
        public Assembly myModuleAssembly;
        Microsoft.Reporting.WinForms.ReportViewer reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public MainForm() {
            TaskbarHelper.InitDemoJumpList(TaskbarAssistant.Default, this);
            Program.MainForm = this;
            ShowSplashScreen();
            InitializeComponent();
            DevExpress.Utils.About.UAlgo.Default.DoEventObject(DevExpress.Utils.About.UAlgo.kDemo, DevExpress.Utils.About.UAlgo.pWinForms, this);
        }

        //Show loading screen when the application is still loading
        void ShowSplashScreen() {
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowDefaultWaitForm();
        }
        void MainForm_Load(object sender, EventArgs e) {
            //set the results table height based on screen size
            tasksGridControl.MaximumSize = new System.Drawing.Size(0, this.Height - 310);

            //Check Database and network connection is valid
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (config.AppSettings.Settings["IPAddress"] != null)
            {
                BuidReportViewer();
                loadAppData("Dashboard");
                InitTileBar();
                mainTileBar.SelectedItem = dashboardTileBarItem;
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseDefaultWaitForm();
            }
            else
            {
                //This mean the connection status = disconnected - close loading screen and redirect to the Connection settings screeen
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseDefaultWaitForm();
                this.Hide();
                Form main = new MainForm();
                Form st = new Settings();
                st.Show();
                main.Close();
            }

            
        }

        //Set the tilebars tags - these will be used for click events
        void InitTileBar() {
            ipcTileBarItem.Tag = "IPC";
            applicationSettingsTileBarItem.Tag = "AppSettings";
            reportsTileBarItem.Tag = "Reports";
            dashboardTileBarItem.Tag = "Dashboard";
        }

        //When close icon is clicked, exit the application
        void navButtonClose_ElementClick(object sender, NavElementEventArgs e) {
            Application.Exit();
        }

        //Hide tilebar dropdown
        private void productTileBar_ItemClick(object sender, TileItemEventArgs e) {
            mainTileBar.HideDropDownWindow();
        }

        //Hide tilebar dropdown
        private void customTileBar_ItemClick(object sender, TileItemEventArgs e) {
            mainTileBar.HideDropDownWindow();
        }

        //prepare datagrid for new data source
        public void PrepareForGridUpdate() 
        {
            tasksGridControl.BeginUpdate();
            tasksGridView.Columns.Clear();
            tasksGridControl.DataSource = null;
        }

        //close datagrid after a new datasource
        public void GridUpdateCompleted() {
            tasksGridView.Columns[0].Visible = false;
            tasksGridControl.EndUpdate();
        }

        //load new data every 30 seconds based on selected filter criteria
        public void DashboardAsyncData(object sender, System.EventArgs e)
        {
            tasksGridControl.MaximumSize = new System.Drawing.Size(0, this.Height - 310);

            //Get today's 6am, eg 2016-07-06 06:00
            var yesterday6am = DateTime.Now.Date.AddHours(6);
            var nowDate = DateTime.Now;

            //if the current time is less than today's 6am, then get yesterday's 6am(this control shifts 6 to 6)
            if (nowDate < yesterday6am)
                 yesterday6am = DateTime.Now.Date.AddDays(-1).AddHours(6);
            try
            {
                using (var sovDB = new PackToPalleteEntities())
                {
                    //get confirmed pallets total, removed pallets total, inactive pallets total e.t.c
                    var appQuery = (from b in sovDB.IPCDisplays select b).ToList();
                    tileItemAll.Text = (from b in sovDB.Pallets
                                        where b.LastUpdated >= yesterday6am
                                        select b).Count().ToString();
                    tileItemInProgress.Text = (from b in sovDB.Pallets
                                               where b.CurrentPackCount == b.MaxPacks && b.IsComplete == true && b.LastUpdated >= yesterday6am
                                               select b).Count().ToString();
                    tileItemNotStartedTask.Text = (from b in sovDB.Pallets
                                                   where b.Removed == true && b.LastUpdated >= yesterday6am
                                                   select b).Count().ToString();
                    tileItemCompleted.Text = (from b in sovDB.Pallets
                                              where ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) > 0 || ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) > 13)) &&
                                                   b.IsComplete == false && b.Removed == false &&
                                                   b.LastUpdated >= yesterday6am
                                              select b).Count().ToString();


                    //show all today's pallets
                    if (tableFilter == 0)
                    {
                        var query = (from b in sovDB.Pallets
                                     join ipc in sovDB.IPCDisplays on b.DisplayNo equals ipc.DisplayNo
                                     where b.LastUpdated >= yesterday6am
                                     orderby b.PalletID
                                     select new
                                     {
                                         DisplayNo = b.DisplayNo,
                                         PalleteNumber = b.PalletNumber,
                                         Display = ipc.DisplayName,
                                         Plu = b.Plu,
                                         Description = b.Description,
                                         DateReceived = b.DateReceived,
                                         Packs = b.CurrentPackCount,
                                         Mass = b.CurrentPackWeight + " Kg",
                                         TimeOnFloor = ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60)),
                                         LastActive = ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago." : (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago."),
                                         Complete = (((double)b.CurrentPackCount) / ((double)(b.MaxPacks??100)) * 100) + " %"

                                     }).ToList();
                     
                        PrepareForGridUpdate();
                        tasksGridControl.DataSource = query;
                        GridUpdateCompleted();
                    }
                    else
                    {
                        //Get all today's confirmed pallets
                        if (tableFilter == 1)
                        {
                            var query = (from b in sovDB.Pallets
                                         join ipc in sovDB.IPCDisplays on b.DisplayNo equals ipc.DisplayNo
                                         where b.CurrentPackCount == b.MaxPacks && b.IsComplete == true
                                         &&
                                         b.LastUpdated >= yesterday6am
                                         orderby b.PalletID
                                         select new
                                         {
                                             DisplayNo = b.DisplayNo,
                                             PalleteNumber = b.PalletNumber,
                                             Display = ipc.DisplayName,
                                             Plu = b.Plu,
                                             Description = b.Description,
                                             DateReceived = b.DateReceived,
                                             Packs = b.CurrentPackCount,
                                             Mass = b.CurrentPackWeight + " Kg",
                                             TimeOnFloor = ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60)),
                                             LastActive = ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago." : (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago."),
                                             Complete = (((double)b.CurrentPackCount) / ((double)(b.MaxPacks ?? 100)) * 100) + " %"

                                         }).ToList();
                            PrepareForGridUpdate();
                            tasksGridControl.DataSource = query;
                            GridUpdateCompleted();
                        }
                            //Get all today's removed pallets
                        else if (tableFilter == 2)
                        {
                            var query = (from b in sovDB.Pallets
                                         join ipc in sovDB.IPCDisplays on b.DisplayNo equals ipc.DisplayNo
                                         where b.Removed == true &&
                                         b.LastUpdated >= yesterday6am
                                         orderby b.PalletID
                                         select new
                                         {
                                             DisplayNo = b.DisplayNo,
                                             PalleteNumber = b.PalletNumber,
                                             Display = ipc.DisplayName,
                                             Plu = b.Plu,
                                             Description = b.Description,
                                             DateReceived = b.DateReceived,
                                             Packs = b.CurrentPackCount,
                                             Mass = b.CurrentPackWeight + " Kg",
                                             TimeOnFloor = ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60)),
                                             LastActive = ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago." : (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago."),
                                             Complete = (((double)b.CurrentPackCount) / ((double)(b.MaxPacks ?? 100)) * 100) + " %"

                                         }).ToList();
                            PrepareForGridUpdate();
                            tasksGridControl.DataSource = query;
                            GridUpdateCompleted();
                        }
                            //Get all today's Inactive pallets
                        else if (tableFilter == 3)
                        {
                            var query = (from b in sovDB.Pallets
                                         join ipc in sovDB.IPCDisplays on b.DisplayNo equals ipc.DisplayNo
                                         where ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) > 0 || ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) > 13)) &&
                                         b.IsComplete == false && b.Removed == false &&
                                         b.LastUpdated >= yesterday6am
                                         orderby b.PalletID
                                         select new
                                         {
                                             DisplayNo = b.DisplayNo,
                                             PalleteNumber = b.PalletNumber,
                                             Display = ipc.DisplayName,
                                             Plu = b.Plu,
                                             Description = b.Description,
                                             DateReceived = b.DateReceived,
                                             Packs = b.CurrentPackCount,
                                             Mass = b.CurrentPackWeight + " Kg",
                                             TimeOnFloor = ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.DateReceived, SqlFunctions.GetDate()) % 60)),
                                             LastActive = ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60) : "" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) / 60)) + ":" + ((SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) < 10 ? "0" + (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago." : (SqlFunctions.DateDiff("MINUTE", b.LastUpdated, SqlFunctions.GetDate()) % 60) + " min ago."),
                                             Complete = (((double)b.CurrentPackCount) / ((double)(b.MaxPacks ?? 100)) * 100) + " %"

                                         }).ToList();
                            PrepareForGridUpdate();
                            tasksGridControl.DataSource = query;
                            GridUpdateCompleted();
                        }

                        
                    }

                    //Refresh the results table with the new info
                    tileSettings("Dashboard");
                }
                //set refresh time to 30 seconds
                dispatcherTimer.Interval = new TimeSpan(0, 0, 30);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //this will be excuted when "Dashboard", "App Settings", "Display Settings" and "Reports" is clicked
        public void loadAppData(string clickedBar)
        {
            tasksGridControl.MaximumSize = new System.Drawing.Size(0, this.Height - 310);
            dispatcherTimer.Stop();

            //Show print button and hide Disable button
            windowsUIButtonPanel1.Buttons[1].Properties.Visible = true;
            windowsUIButtonPanel1.Buttons[0].Properties.Visible = false;
            windowsUIButtonPanel1.ButtonInterval = 10;

            //Get dashboard data and refresh it every nth seconds
            if (clickedBar == "Dashboard")
            {
                if (this.Controls.IndexOf(reportViewer2) > -1)
                    BodySettings("");
                dispatcherTimer.Tick += new EventHandler(DashboardAsyncData);
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0);
                dispatcherTimer.Start();
            }
            else
            {
                using (var sovDB = new PackToPalleteEntities())
                {
                    switch (clickedBar)
                    {
                       //Show application settings window
                        case "AppSettings":
                            this.Hide();
                            Form main = new MainForm();
                            Form st = new Settings();
                            st.Show();
                            main.Close();
                            break;

                        //Load the results table with displays data source
                        case "IPC":
                            windowsUIButtonPanel1.Buttons[0].Properties.Visible = true;
                            windowsUIButtonPanel1.ButtonInterval = 100;
                            if (this.Controls.IndexOf(reportViewer2) > -1)
                                BodySettings("");
                            try
                            {
                                var appQuery = (from b in sovDB.IPCDisplays
                                                select b).ToList();
                                PrepareForGridUpdate();
                                tasksGridControl.DataSource = appQuery;
                                GridUpdateCompleted();
                                tasksGridView.Columns[4].Visible = false;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            tileSettings(clickedBar);
                            break;

                        //Show reports viewer, hide results table and print button
                        case "Reports":
                            windowsUIButtonPanel1.Buttons[1].Properties.Visible = false;
                            if(this.Controls.IndexOf(dataLayoutControl1) > -1)
                                BodySettings("report");

                            break;
                        default:
                            break;
                    }

                }
                
            }
        }

        //Build a report view
        public void BuidReportViewer()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            reportViewer2.Dock = System.Windows.Forms.DockStyle.Fill;
            reportViewer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
          
            reportViewer2.Location = new System.Drawing.Point(20, (tasksGridControl.Location.Y + dataLayoutControl1.Location.Y + 5));
            reportViewer2.Name = "reportViewer1";
            reportViewer2.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote;
            reportViewer2.ServerReport.ReportPath = config.AppSettings.Settings["Path"].Value.ToString() + "/p2p_Stale_Pallets";
            string hosti = "http://" + config.AppSettings.Settings["ReportsServer"].Value.ToString() + "/" + config.AppSettings.Settings["VirtualDirectory"].Value.ToString();
            reportViewer2.ServerReport.ReportServerUrl = new System.Uri(hosti, System.UriKind.Absolute);
            reportViewer2.Size = new System.Drawing.Size(this.Width - 70, this.Height - 310);
            reportViewer2.TabIndex = 27;
            
        }

        //toggle between datagrid and reportviewer
        public void BodySettings(string bodyContent)
        {
            switch(bodyContent)
            {
                case "report":
                    this.Controls.Remove(dataLayoutControl1);
                    this.Controls.Add(reportViewer2);
                    this.Controls.SetChildIndex(reportViewer2, 1);
                    reportViewer2.RefreshReport();
                    
                    
                    break;
                default:
                    this.Controls.Remove(reportViewer2);
                    this.Controls.Add(dataLayoutControl1);
                    this.Controls.SetChildIndex(dataLayoutControl1, 1);
                 break;
            }
        }

        //toggle left menu tiles
        public void tileSettings(string clickedBar)
        {
            ipcFilter = 0;
            switch (clickedBar)
            {
                //if Dashboard is clicked, show all the tilebars
                case "Dashboard":
                    tileItemAll.Elements[1].Text = "All Pallets";
                    tileItemInProgress.Elements[1].Text = "Confirmed Pallets";
                    tileItemAll.Image = global::DevExpress.SovFoods.Properties.Resources.AllTasks;
                    tasksSLI.Text = "Tasks";
                    tileGroup2.Items.Clear();
                    tileGroup2.Items.Add(tileItemAll);
                    tileGroup2.Items.Add(tileItemInProgress);
                    tileGroup2.Items.Add(tileItemNotStartedTask);
                    tileGroup2.Items.Add(tileItemCompleted);
                    windowsUIButtonPanel1.Buttons[1].Properties.Caption = "Print";
                    ipcFilter = 0;
                    break;

                //if IPC is clicked, show only Add New Display tile
                case "IPC":
                    tileItemAll.Elements[0].Text = "";
                    tileItemAll.Elements[1].Text = "Add New Display";
                    tileItemAll.Image = global::DevExpress.SovFoods.Properties.Resources.Monitors;
                    tasksSLI.Text = "Display Settings";
                    tileGroup2.Items.Clear();
                    tileGroup2.Items.Add(tileItemAll);
                    windowsUIButtonPanel1.Buttons[1].Properties.Caption = "Save";
                    ipcFilter = 1;
                    break;
                default:
                    break;
            }
        }

        //this will be executed when "Dashboard", "App Settings", "IPC Display" or "Reports" is clicked
        private void mainTileBar_SelectedItemChanged(object sender, TileItemEventArgs e) {
            loadAppData(e.Item.Tag.ToString());
        }

        //Help button is visible, but we are not doing anything when it's clicked yet
        void navButtonHelp_ElementClick(object sender, NavElementEventArgs e) {
        }

        //Conformed Pallets Filter
        private void tileItemInProgress_ItemClick(object sender, TileItemEventArgs e)
        {
            tableFilter = 1;
            loadAppData("Dashboard");
        }

        //Removed Pallets Filter
        private void tileItemNotStartedTask_ItemClick(object sender, TileItemEventArgs e)
        {
            tableFilter = 2;
            loadAppData("Dashboard");
        }

        //Show all pallets
        private void tileItemAll_ItemClick(object sender, TileItemEventArgs e)
        {
            //if Dashboard tile is clicked, load dashboard data
            if (ipcFilter == 0)
            {
                tableFilter = 0;
                loadAppData("Dashboard");
            }
            //if Add New Display is clicked, show display screen
            else
            {
                this.Hide();
                Form main = new MainForm();
                Form st = new Displays();
                st.Show();
                main.Close();
            }
        }
        private static bool canCloseFunc(DialogResult parameter)
        {
            return parameter != DialogResult.Cancel;
        }

        //Handle bottom panel buttons events
        private void windowsUIButtonPanel1_ButtonClick(object sender, XtraBars.Docking2010.ButtonEventArgs e)
        {
            switch(e.Button.Properties.Caption)
            {
                case "Print":
                    //Printing
                    tasksGridView.ShowPrintPreview();

                    break;
                case "Enable/Disable":
                    try
                    {
                        using (var sovDB = new PackToPalleteEntities())
                        {
                            int displayId = int.Parse(tasksGridView.GetRowCellValue(tasksGridView.FocusedRowHandle, "DisplayNo").ToString());
                            int state = 1;
                            string stateDesc = "Enabled";
                            if (int.Parse(tasksGridView.GetRowCellValue(tasksGridView.FocusedRowHandle, "State").ToString()) == 1)
                            {
                                state = 0;
                                stateDesc = "Disabled";
                            }
                            var result = sovDB.IPCDisplays.SingleOrDefault(b => b.DisplayNo == displayId);
                            if (result != null)
                            {
                                result.State = state;
                                result.StateDescription = stateDesc;
                                sovDB.SaveChanges();
                                loadAppData("IPC");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "Save":
                    try
                    {
                        using (var sovDB = new PackToPalleteEntities())
                        {
                            for (int i = 0; i < tasksGridView.DataRowCount; i++)
                            {
                                int displayId = int.Parse(tasksGridView.GetRowCellValue(i, "DisplayNo").ToString());
                                var result = sovDB.IPCDisplays.SingleOrDefault(b => b.DisplayNo == displayId);
                                int isChecked = int.Parse(tasksGridView.GetRowCellDisplayText(i, "State").ToString());

                                if (result != null)
                                {
                                    result.DisplayName = tasksGridView.GetRowCellValue(i, "DisplayName").ToString();
                                    result.Description = tasksGridView.GetRowCellValue(i, "Description").ToString();
                                    result.IPAddress = tasksGridView.GetRowCellValue(i, "IPAddress").ToString();
                                    sovDB.SaveChanges();
                                }
                            }
                            loadAppData("IPC");
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    
                    
                    break;
                default:
                    break;
            }
        }

        //Show Stall Pallets Report
        private void StalePalletsReport_ItemClick(object sender, TileItemEventArgs e)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            reportViewer2.ServerReport.ReportPath = config.AppSettings.Settings["Path"].Value.ToString() + "/p2p_Stale_Pallets";
            reportViewer2.RefreshReport();
        }

        //Show Exception Report
        private void exeptionReport_ItemClick(object sender, TileItemEventArgs e)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            reportViewer2.ServerReport.ReportPath = config.AppSettings.Settings["Path"].Value.ToString() + "/p2p_Exception_Report";
            reportViewer2.RefreshReport();
        }

        //Show full Report
        private void fullReport_ItemClick(object sender, TileItemEventArgs e)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            reportViewer2.ServerReport.ReportPath = config.AppSettings.Settings["Path"].Value.ToString() + "/p2p_Full_Report";
            reportViewer2.RefreshReport();
        }

        //show all inactive pallets
        private void tileItemCompleted_ItemClick(object sender, TileItemEventArgs e)
        {
            tableFilter = 3;
            loadAppData("Dashboard");
        }

        //if the user tried to edit State Description, cancel that event
        private void tasksGridView_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedColumn.FieldName == "StateDescription")
            {
                e.Cancel = true;
            }
        }

    }
}
