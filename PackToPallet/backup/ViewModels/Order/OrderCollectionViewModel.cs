﻿//using System;
//using System.Linq;
//using DevExpress.Mvvm.POCO;
//using DevExpress.SovFoods.Common.Utils;
//using DevExpress.SovFoods.DevAVDbDataModel;
//using DevExpress.SovFoods.Common.DataModel;
//using DevExpress.SovFoods;
//using DevExpress.SovFoods.Common.ViewModel;
//using DevExpress.Mvvm;
//using System.Collections.Generic;
//using System.Data.Entity;

//namespace DevExpress.SovFoods.ViewModels {
//    /// <summary>
//    /// Represents the Orders collection view model.
//    /// </summary>
//    public partial class OrderCollectionViewModel : CollectionViewModel<Order, long, IDevAVDbUnitOfWork> {

//        /// <summary>
//        /// Initializes a new instance of the OrderCollectionViewModel class.
//        /// This constructor is declared protected to avoid undesired instantiation of the OrderCollectionViewModel type without the POCO proxy factory.
//        /// </summary>
//        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
//        protected OrderCollectionViewModel(IUnitOfWorkFactory<IDevAVDbUnitOfWork> unitOfWorkFactory = null)
//            : base(unitOfWorkFactory, x => x.Orders) {
//        }
//        public OrderCollectionViewModel()
//            : this(DbUnitOfWorkFactory.Instance) {
//        }
//    }
//}
