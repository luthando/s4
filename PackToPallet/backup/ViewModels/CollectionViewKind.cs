﻿namespace DevExpress.SovFoods.ViewModels {
    public enum CollectionViewKind {
        ListView,
        CardView,
        Carousel
    }
}
