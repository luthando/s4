﻿//using System;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using DevExpress.Mvvm;
//using DevExpress.Mvvm.POCO;
//using DevExpress.SovFoods.Common.Utils;
//using DevExpress.SovFoods.DevAVDbDataModel;
//using DevExpress.SovFoods.Common.DataModel;
//using DevExpress.SovFoods;
//using DevExpress.SovFoods.Common.ViewModel;

//namespace DevExpress.SovFoods.ViewModels {
//    /// <summary>
//    /// Represents the single Quote object view model.
//    /// </summary>
//    public partial class QuoteViewModel : SingleObjectViewModel<Quote, long, IDevAVDbUnitOfWork> {

//        /// <summary>
//        /// Initializes a new instance of the QuoteViewModel class.
//        /// This constructor is declared protected to avoid undesired instantiation of the QuoteViewModel type without the POCO proxy factory.
//        /// </summary>
//        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
//        protected QuoteViewModel(IUnitOfWorkFactory<IDevAVDbUnitOfWork> unitOfWorkFactory = null)
//            : base(unitOfWorkFactory, x => x.Quotes, x => x.Number) {
//        }
//        public QuoteViewModel()
//            : this(DbUnitOfWorkFactory.Instance) {
//        }


//        /// <summary>
//        /// The look-up collection of Employees for the corresponding navigation property in the view.
//        /// </summary>
//        public IList<Employee> LookUpEmployees {
//            get { return GetLookUpEntities(x => x.Employees); }
//        }

//        protected override void RefreshLookUpCollections(long key) {
//            QuoteQuoteItemsLookUp = CreateLookUpCollectionViewModel(x => x.QuoteItems, x => x.QuoteId, (x, m) => x.Quote = m, key);
//        }

//        /// <summary>
//        /// The view model for the QuoteQuoteItems detail collection.
//        /// </summary>
//        public virtual CollectionViewModel<QuoteItem, long, IDevAVDbUnitOfWork> QuoteQuoteItemsLookUp { get; set; }
//    }
//}
