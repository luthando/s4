﻿//using System;
//using System.Linq;
//using DevExpress.Mvvm.POCO;
//using DevExpress.SovFoods.Common.Utils;
//using DevExpress.SovFoods.DevAVDbDataModel;
//using DevExpress.SovFoods.Common.DataModel;
//using DevExpress.SovFoods;
//using DevExpress.SovFoods.Common.ViewModel;
//using System.Collections.Generic;
//using System.Linq.Expressions;
//using DevExpress.Mvvm;
//using System.Data.Entity;

//namespace DevExpress.SovFoods.ViewModels {
//    /// <summary>
//    /// Represents the Quotes collection view model.
//    /// </summary>
//    public partial class QuoteCollectionViewModel : CollectionViewModel<Quote, long, IDevAVDbUnitOfWork> {

//        /// <summary>
//        /// Initializes a new instance of the QuoteCollectionViewModel class.
//        /// This constructor is declared protected to avoid undesired instantiation of the QuoteCollectionViewModel type without the POCO proxy factory.
//        /// </summary>
//        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
//        protected QuoteCollectionViewModel(IUnitOfWorkFactory<IDevAVDbUnitOfWork> unitOfWorkFactory = null)
//            : base(unitOfWorkFactory, x => x.Quotes) {
//        }
//        public QuoteCollectionViewModel()
//            : this(DbUnitOfWorkFactory.Instance) {
//        }
//    }
//}
