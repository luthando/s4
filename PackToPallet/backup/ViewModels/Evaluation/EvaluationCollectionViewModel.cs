﻿//using System;
//using System.Linq;
//using DevExpress.Mvvm.POCO;
//using DevExpress.SovFoods.Common.Utils;
//using DevExpress.SovFoods.DevAVDbDataModel;
//using DevExpress.SovFoods.Common.DataModel;
//using DevExpress.SovFoods;
//using DevExpress.SovFoods.Common.ViewModel;

//namespace DevExpress.SovFoods.ViewModels {
//    /// <summary>
//    /// Represents the Evaluations collection view model.
//    /// </summary>
//    public partial class EvaluationCollectionViewModel : CollectionViewModel<Evaluation, long, IDevAVDbUnitOfWork> {

//        /// <summary>
//        /// Initializes a new instance of the EvaluationCollectionViewModel class.
//        /// This constructor is declared protected to avoid undesired instantiation of the EvaluationCollectionViewModel type without the POCO proxy factory.
//        /// </summary>
//        /// <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
//        protected EvaluationCollectionViewModel(IUnitOfWorkFactory<IDevAVDbUnitOfWork> unitOfWorkFactory = null)
//            : base(unitOfWorkFactory, x => x.Evaluations) {
//        }
//        public EvaluationCollectionViewModel()
//            : this(DbUnitOfWorkFactory.Instance) {
//        }
//    }
//}
