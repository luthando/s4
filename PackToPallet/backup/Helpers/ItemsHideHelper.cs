﻿using System.Collections;
using System.Drawing;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;

namespace DevExpress.SovFoods.Helpers {
    public static class ArrowsHelper {
        static Image arrowLeftCore;
        public static Image ArrowLeft {
            get {
                if(arrowLeftCore == null)
                    arrowLeftCore = ImageFromResource("ArrowLeft.png");
                return arrowLeftCore;
            }
        }
        static Image arrowRightCore;
        public static Image ArrowRight {
            get {
                if(arrowRightCore == null)
                    arrowRightCore = ImageFromResource("ArrowRight.png");
                return arrowRightCore;
            }
        }
        static Image ImageFromResource(string resourceName) {
            return DevExpress.Utils.ResourceImageHelper.CreateImageFromResources("DevExpress.SovFoods.Resources." + resourceName, typeof(ArrowsHelper).Assembly);
        }
    }
    public static class ItemsHideHelper {
        public static void Hide(ICollection baseItemCollection, SimpleLabelItem button) {
            Hide(baseItemCollection, button, false);
        }
        public static void Hide(ICollection baseItemCollection, SimpleLabelItem button, bool conversely) {
            foreach(BaseLayoutItem bli in baseItemCollection)
                bli.Visibility = LayoutVisibility.Never;
            button.Image = conversely ? ArrowsHelper.ArrowLeft : ArrowsHelper.ArrowRight;
        }
        public static void Expand(ICollection baseItemCollection, SimpleLabelItem button) {
            Expand(baseItemCollection, button, false);
        }
        public static void Expand(ICollection baseItemCollection, SimpleLabelItem button, bool conversely) {
            foreach(BaseLayoutItem bli in baseItemCollection)
                bli.Visibility = LayoutVisibility.Always;
            button.Image = conversely ? ArrowsHelper.ArrowRight : ArrowsHelper.ArrowLeft;
        }
    }
}
