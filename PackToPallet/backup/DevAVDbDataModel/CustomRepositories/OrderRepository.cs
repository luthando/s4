﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using DevExpress.SovFoods.Common.DataModel.EntityFramework;

//namespace DevExpress.SovFoods.DevAVDbDataModel {
//    public class OrderRepository : DbRepository<Order, long, DevAVDb> {
//        public OrderRepository(DbUnitOfWork<DevAVDb> unitOfWork) : base(unitOfWork, x => x.Set<Order>(), x => x.Id) { }
//        protected override IQueryable<Order> GetEntities() {
//            return base.GetEntities().Where(o => o.OrderDate < DateTime.Now);
//        }
//    }
//}
