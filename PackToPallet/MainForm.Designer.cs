﻿using DevExpress.Utils.Animation;
namespace DevExpress.SovFoods {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            DevExpress.Utils.Animation.SlideFadeTransition slideFadeTransition1 = new DevExpress.Utils.Animation.SlideFadeTransition();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            this.modulesContainer = new DevExpress.XtraEditors.XtraUserControl();
            this.tileNavPane = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navButtonHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButtonHelp = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButtonClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.mainTileBar = new DevExpress.XtraBars.Navigation.TileBar();
            this.tileBarGroup3 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.dashboardTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.applicationSettingsTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.ipcTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.reportsTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.productsTileBarDropDownContainter = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.productTileBar = new DevExpress.XtraBars.Navigation.TileBar();
            this.tileBarGroup1 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.StalePalletsReport = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.exeptionReport = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.fullReport = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.customTileBarDropDownContainter = new DevExpress.XtraBars.Navigation.TileBarDropDownContainer();
            this.customTileBar = new DevExpress.XtraBars.Navigation.TileBar();
            this.tileBarGroup4 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.AllStoresTBI = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.customMyAccountTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.customJohnAccountTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.customTopStoresTileBarItem = new DevExpress.XtraBars.Navigation.TileBarItem();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager();
            this.flyoutPanel1 = new DevExpress.Utils.FlyoutPanel();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileItemAll = new DevExpress.XtraEditors.TileItem();
            this.tileItemInProgress = new DevExpress.XtraEditors.TileItem();
            this.tileItemNotStartedTask = new DevExpress.XtraEditors.TileItem();
            this.tileItemCompleted = new DevExpress.XtraEditors.TileItem();
            this.tasksGridControl = new DevExpress.XtraGrid.GridControl();
            this.tasksGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tileControlLCI = new DevExpress.XtraLayout.LayoutControlItem();
            this.buttonHide = new DevExpress.XtraLayout.SimpleLabelItem();
            this.tasksSLI = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.icBackground = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.productsTileBarDropDownContainter)).BeginInit();
            this.productsTileBarDropDownContainter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customTileBarDropDownContainter)).BeginInit();
            this.customTileBarDropDownContainter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tasksGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileControlLCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksSLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icBackground)).BeginInit();
            this.SuspendLayout();
            // 
            // modulesContainer
            // 
            this.modulesContainer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.modulesContainer.Appearance.Options.UseBackColor = true;
            this.modulesContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modulesContainer.Location = new System.Drawing.Point(0, 170);
            this.modulesContainer.Name = "modulesContainer";
            this.modulesContainer.Size = new System.Drawing.Size(1333, 563);
            this.modulesContainer.TabIndex = 0;
            // 
            // tileNavPane
            // 
            this.tileNavPane.AllowGlyphSkinning = true;
            this.tileNavPane.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.tileNavPane.Appearance.Options.UseBackColor = true;
            this.tileNavPane.AppearanceHovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(212)))), ((int)(((byte)(219)))));
            this.tileNavPane.AppearanceHovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.tileNavPane.AppearanceHovered.Options.UseBackColor = true;
            this.tileNavPane.AppearanceHovered.Options.UseForeColor = true;
            this.tileNavPane.AppearanceSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(212)))), ((int)(((byte)(219)))));
            this.tileNavPane.AppearanceSelected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.tileNavPane.AppearanceSelected.Options.UseBackColor = true;
            this.tileNavPane.AppearanceSelected.Options.UseForeColor = true;
            this.tileNavPane.ButtonPadding = new System.Windows.Forms.Padding(12);
            this.tileNavPane.Buttons.Add(this.navButtonHome);
            this.tileNavPane.Buttons.Add(this.navButtonHelp);
            this.tileNavPane.Buttons.Add(this.navButtonClose);
            // 
            // tileNavCategory1
            // 
            this.tileNavPane.DefaultCategory.Name = "tileNavCategory1";
            this.tileNavPane.DefaultCategory.OptionsDropDown.BackColor = System.Drawing.Color.Empty;
            this.tileNavPane.DefaultCategory.OwnerCollection = null;
            // 
            // 
            // 
            this.tileNavPane.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileNavPane.DefaultCategory.Tile.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Default;
            this.tileNavPane.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileNavPane.Location = new System.Drawing.Point(0, 0);
            this.tileNavPane.Name = "tileNavPane";
            this.tileNavPane.OptionsPrimaryDropDown.BackColor = System.Drawing.Color.Empty;
            this.tileNavPane.OptionsSecondaryDropDown.BackColor = System.Drawing.Color.Empty;
            this.tileNavPane.Size = new System.Drawing.Size(1333, 61);
            this.tileNavPane.TabIndex = 0;
            this.tileNavPane.Text = "tileNavPane1";
            // 
            // navButtonHome
            // 
            this.navButtonHome.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Left;
            this.navButtonHome.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.navButtonHome.Caption = null;
            this.navButtonHome.Enabled = false;
            this.navButtonHome.Glyph = ((System.Drawing.Image)(resources.GetObject("navButtonHome.Glyph")));
            this.navButtonHome.Name = "navButtonHome";
            this.navButtonHome.Padding = new System.Windows.Forms.Padding(2, -1, -1, 2);
            // 
            // navButtonHelp
            // 
            this.navButtonHelp.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButtonHelp.Caption = null;
            this.navButtonHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("navButtonHelp.Glyph")));
            this.navButtonHelp.Name = "navButtonHelp";
            this.navButtonHelp.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navButtonHelp_ElementClick);
            // 
            // navButtonClose
            // 
            this.navButtonClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButtonClose.Caption = null;
            this.navButtonClose.Glyph = ((System.Drawing.Image)(resources.GetObject("navButtonClose.Glyph")));
            this.navButtonClose.Name = "navButtonClose";
            this.navButtonClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navButtonClose_ElementClick);
            // 
            // mainTileBar
            // 
            this.mainTileBar.AllowDrag = false;
            this.mainTileBar.AllowSelectedItem = true;
            this.mainTileBar.AppearanceGroupText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.mainTileBar.AppearanceGroupText.Options.UseForeColor = true;
            this.mainTileBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.mainTileBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainTileBar.DropDownButtonWidth = 30;
            this.mainTileBar.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.mainTileBar.Groups.Add(this.tileBarGroup3);
            this.mainTileBar.IndentBetweenGroups = 10;
            this.mainTileBar.IndentBetweenItems = 10;
            this.mainTileBar.ItemPadding = new System.Windows.Forms.Padding(8, 6, 12, 6);
            this.mainTileBar.Location = new System.Drawing.Point(0, 61);
            this.mainTileBar.MaxId = 8;
            this.mainTileBar.Name = "mainTileBar";
            this.mainTileBar.Padding = new System.Windows.Forms.Padding(21, 8, 21, 20);
            this.mainTileBar.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.mainTileBar.SelectedItem = this.dashboardTileBarItem;
            this.mainTileBar.SelectionBorderWidth = 2;
            this.mainTileBar.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.mainTileBar.Size = new System.Drawing.Size(1333, 109);
            this.mainTileBar.TabIndex = 1;
            this.mainTileBar.Text = "tileBar1";
            this.mainTileBar.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.mainTileBar.WideTileWidth = 206;
            this.mainTileBar.SelectedItemChanged += new DevExpress.XtraEditors.TileItemClickEventHandler(this.mainTileBar_SelectedItemChanged);
            // 
            // tileBarGroup3
            // 
            this.tileBarGroup3.Items.Add(this.dashboardTileBarItem);
            this.tileBarGroup3.Items.Add(this.applicationSettingsTileBarItem);
            this.tileBarGroup3.Items.Add(this.ipcTileBarItem);
            this.tileBarGroup3.Items.Add(this.reportsTileBarItem);
            this.tileBarGroup3.Name = "tileBarGroup3";
            this.tileBarGroup3.Text = "OPERATIONS";
            // 
            // dashboardTileBarItem
            // 
            this.dashboardTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.dashboardTileBarItem.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(135)))), ((int)(((byte)(156)))));
            this.dashboardTileBarItem.AppearanceItem.Normal.Options.UseBackColor = true;
            this.dashboardTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement1.Text = "Dashboard";
            this.dashboardTileBarItem.Elements.Add(tileItemElement1);
            this.dashboardTileBarItem.Id = 0;
            this.dashboardTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.dashboardTileBarItem.Name = "dashboardTileBarItem";
            // 
            // applicationSettingsTileBarItem
            // 
            this.applicationSettingsTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.applicationSettingsTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement2.Text = "Application Settings";
            this.applicationSettingsTileBarItem.Elements.Add(tileItemElement2);
            this.applicationSettingsTileBarItem.Id = 1;
            this.applicationSettingsTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.applicationSettingsTileBarItem.Name = "applicationSettingsTileBarItem";
            // 
            // ipcTileBarItem
            // 
            this.ipcTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.ipcTileBarItem.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(109)))), ((int)(((byte)(0)))));
            this.ipcTileBarItem.AppearanceItem.Normal.Options.UseBackColor = true;
            this.ipcTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement3.Text = "Displays";
            this.ipcTileBarItem.Elements.Add(tileItemElement3);
            this.ipcTileBarItem.Id = 2;
            this.ipcTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.ipcTileBarItem.Name = "ipcTileBarItem";
            // 
            // reportsTileBarItem
            // 
            this.reportsTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.reportsTileBarItem.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(196)))));
            this.reportsTileBarItem.AppearanceItem.Normal.Options.UseBackColor = true;
            this.reportsTileBarItem.DropDownControl = this.productsTileBarDropDownContainter;
            this.reportsTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement7.Text = "Reports";
            this.reportsTileBarItem.Elements.Add(tileItemElement7);
            this.reportsTileBarItem.Id = 3;
            this.reportsTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.reportsTileBarItem.Name = "reportsTileBarItem";
            // 
            // productsTileBarDropDownContainter
            // 
            this.productsTileBarDropDownContainter.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(196)))));
            this.productsTileBarDropDownContainter.Appearance.Options.UseBackColor = true;
            this.productsTileBarDropDownContainter.Controls.Add(this.productTileBar);
            this.productsTileBarDropDownContainter.Location = new System.Drawing.Point(0, 0);
            this.productsTileBarDropDownContainter.Name = "productsTileBarDropDownContainter";
            this.productsTileBarDropDownContainter.Size = new System.Drawing.Size(931, 120);
            this.productsTileBarDropDownContainter.TabIndex = 3;
            // 
            // productTileBar
            // 
            this.productTileBar.AllowDrag = false;
            this.productTileBar.AllowSelectedItem = true;
            this.productTileBar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.productTileBar.AppearanceGroupText.ForeColor = System.Drawing.Color.White;
            this.productTileBar.AppearanceGroupText.Options.UseForeColor = true;
            this.productTileBar.AppearanceItem.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
            this.productTileBar.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.Black;
            this.productTileBar.AppearanceItem.Hovered.Options.UseBackColor = true;
            this.productTileBar.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.productTileBar.AppearanceItem.Normal.BackColor = System.Drawing.Color.White;
            this.productTileBar.AppearanceItem.Normal.ForeColor = System.Drawing.Color.Black;
            this.productTileBar.AppearanceItem.Normal.Options.UseBackColor = true;
            this.productTileBar.AppearanceItem.Normal.Options.UseForeColor = true;
            this.productTileBar.AppearanceItem.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(149)))), ((int)(((byte)(201)))));
            this.productTileBar.AppearanceItem.Pressed.Options.UseBackColor = true;
            this.productTileBar.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(163)))), ((int)(((byte)(217)))));
            this.productTileBar.AppearanceItem.Selected.ForeColor = System.Drawing.Color.White;
            this.productTileBar.AppearanceItem.Selected.Options.UseBackColor = true;
            this.productTileBar.AppearanceItem.Selected.Options.UseForeColor = true;
            this.productTileBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(196)))));
            this.productTileBar.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.productTileBar.Groups.Add(this.tileBarGroup1);
            this.productTileBar.IndentBetweenGroups = 0;
            this.productTileBar.ItemSize = 40;
            this.productTileBar.Location = new System.Drawing.Point(0, 0);
            this.productTileBar.MaxId = 5;
            this.productTileBar.Name = "productTileBar";
            this.productTileBar.Padding = new System.Windows.Forms.Padding(21, 8, 21, 8);
            this.productTileBar.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.productTileBar.SelectedItem = this.StalePalletsReport;
            this.productTileBar.Size = new System.Drawing.Size(931, 120);
            this.productTileBar.TabIndex = 1;
            this.productTileBar.Text = "tileBar4";
            this.productTileBar.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.productTileBar_ItemClick);
            // 
            // tileBarGroup1
            // 
            this.tileBarGroup1.Items.Add(this.StalePalletsReport);
            this.tileBarGroup1.Items.Add(this.exeptionReport);
            this.tileBarGroup1.Items.Add(this.fullReport);
            this.tileBarGroup1.Name = "tileBarGroup1";
            this.tileBarGroup1.Text = "Please select a report";
            // 
            // StalePalletsReport
            // 
            this.StalePalletsReport.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.StalePalletsReport.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Stale Pallets Report";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.StalePalletsReport.Elements.Add(tileItemElement4);
            this.StalePalletsReport.Id = 0;
            this.StalePalletsReport.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.StalePalletsReport.Name = "StalePalletsReport";
            this.StalePalletsReport.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.StalePalletsReport_ItemClick);
            // 
            // exeptionReport
            // 
            this.exeptionReport.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.exeptionReport.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Exception Report";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.exeptionReport.Elements.Add(tileItemElement5);
            this.exeptionReport.Id = 1;
            this.exeptionReport.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.exeptionReport.Name = "exeptionReport";
            this.exeptionReport.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.exeptionReport_ItemClick);
            // 
            // fullReport
            // 
            this.fullReport.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.fullReport.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Full Report";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.fullReport.Elements.Add(tileItemElement6);
            this.fullReport.Id = 2;
            this.fullReport.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.fullReport.Name = "fullReport";
            this.fullReport.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.fullReport_ItemClick);
            // 
            // customTileBarDropDownContainter
            // 
            this.customTileBarDropDownContainter.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            this.customTileBarDropDownContainter.Appearance.Options.UseBackColor = true;
            this.customTileBarDropDownContainter.Controls.Add(this.customTileBar);
            this.customTileBarDropDownContainter.Location = new System.Drawing.Point(0, 0);
            this.customTileBarDropDownContainter.Name = "customTileBarDropDownContainter";
            this.customTileBarDropDownContainter.Size = new System.Drawing.Size(821, 151);
            this.customTileBarDropDownContainter.TabIndex = 2;
            // 
            // customTileBar
            // 
            this.customTileBar.AllowDrag = false;
            this.customTileBar.AllowSelectedItem = true;
            this.customTileBar.AppearanceGroupText.ForeColor = System.Drawing.Color.White;
            this.customTileBar.AppearanceGroupText.Options.UseForeColor = true;
            this.customTileBar.AppearanceItem.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.customTileBar.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.White;
            this.customTileBar.AppearanceItem.Hovered.Options.UseBackColor = true;
            this.customTileBar.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.customTileBar.AppearanceItem.Normal.BackColor = System.Drawing.Color.White;
            this.customTileBar.AppearanceItem.Normal.ForeColor = System.Drawing.Color.Black;
            this.customTileBar.AppearanceItem.Normal.Options.UseBackColor = true;
            this.customTileBar.AppearanceItem.Normal.Options.UseForeColor = true;
            this.customTileBar.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.customTileBar.AppearanceItem.Selected.ForeColor = System.Drawing.Color.White;
            this.customTileBar.AppearanceItem.Selected.Options.UseBackColor = true;
            this.customTileBar.AppearanceItem.Selected.Options.UseForeColor = true;
            this.customTileBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            this.customTileBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customTileBar.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.customTileBar.Groups.Add(this.tileBarGroup4);
            this.customTileBar.IndentBetweenGroups = 0;
            this.customTileBar.ItemSize = 40;
            this.customTileBar.Location = new System.Drawing.Point(0, 0);
            this.customTileBar.MaxId = 4;
            this.customTileBar.Name = "customTileBar";
            this.customTileBar.Padding = new System.Windows.Forms.Padding(21, 8, 21, 8);
            this.customTileBar.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.customTileBar.SelectedItem = this.AllStoresTBI;
            this.customTileBar.Size = new System.Drawing.Size(821, 151);
            this.customTileBar.TabIndex = 0;
            this.customTileBar.Text = "tileBar2";
            this.customTileBar.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.customTileBar_ItemClick);
            // 
            // tileBarGroup4
            // 
            this.tileBarGroup4.Items.Add(this.AllStoresTBI);
            this.tileBarGroup4.Items.Add(this.customMyAccountTileBarItem);
            this.tileBarGroup4.Items.Add(this.customJohnAccountTileBarItem);
            this.tileBarGroup4.Items.Add(this.customTopStoresTileBarItem);
            this.tileBarGroup4.Name = "tileBarGroup4";
            this.tileBarGroup4.Text = "CUSTOM FILTER";
            // 
            // AllStoresTBI
            // 
            this.AllStoresTBI.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "All Customers";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.AllStoresTBI.Elements.Add(tileItemElement8);
            this.AllStoresTBI.Id = 3;
            this.AllStoresTBI.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.AllStoresTBI.Name = "AllStoresTBI";
            // 
            // customMyAccountTileBarItem
            // 
            this.customMyAccountTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.customMyAccountTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "My Accounts";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.customMyAccountTileBarItem.Elements.Add(tileItemElement9);
            this.customMyAccountTileBarItem.Id = 0;
            this.customMyAccountTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.customMyAccountTileBarItem.Name = "customMyAccountTileBarItem";
            // 
            // customJohnAccountTileBarItem
            // 
            this.customJohnAccountTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.customJohnAccountTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "John\'s Accounts";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.customJohnAccountTileBarItem.Elements.Add(tileItemElement10);
            this.customJohnAccountTileBarItem.Id = 1;
            this.customJohnAccountTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.customJohnAccountTileBarItem.Name = "customJohnAccountTileBarItem";
            // 
            // customTopStoresTileBarItem
            // 
            this.customTopStoresTileBarItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.customTopStoresTileBarItem.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Top Stores";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.customTopStoresTileBarItem.Elements.Add(tileItemElement11);
            this.customTopStoresTileBarItem.Id = 2;
            this.customTopStoresTileBarItem.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.Wide;
            this.customTopStoresTileBarItem.Name = "customTopStoresTileBarItem";
            // 
            // transitionManager1
            // 
            this.transitionManager1.ShowWaitingIndicator = false;
            transition1.Control = this.modulesContainer;
            slideFadeTransition1.Parameters.Background = System.Drawing.Color.Empty;
            slideFadeTransition1.Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            slideFadeTransition1.Parameters.FrameInterval = 5000;
            transition1.TransitionType = slideFadeTransition1;
            this.transitionManager1.Transitions.Add(transition1);
            // 
            // flyoutPanel1
            // 
            this.flyoutPanel1.Location = new System.Drawing.Point(0, 800);
            this.flyoutPanel1.MaximumSize = new System.Drawing.Size(0, 80);
            this.flyoutPanel1.MinimumSize = new System.Drawing.Size(0, 80);
            this.flyoutPanel1.Name = "flyoutPanel1";
            this.flyoutPanel1.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.Bottom;
            this.flyoutPanel1.Options.CloseOnOuterClick = true;
            this.flyoutPanel1.OptionsButtonPanel.ButtonPanelLocation = DevExpress.Utils.FlyoutPanelButtonPanelLocation.Bottom;
            this.flyoutPanel1.OwnerControl = this;
            this.flyoutPanel1.Size = new System.Drawing.Size(0, 80);
            this.flyoutPanel1.TabIndex = 5;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1333, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 733);
            this.barDockControlBottom.Size = new System.Drawing.Size(1333, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 733);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1333, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 733);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.AllowCustomization = false;
            this.dataLayoutControl1.BackColor = System.Drawing.Color.White;
            this.dataLayoutControl1.Controls.Add(this.tileControl1);
            this.dataLayoutControl1.Controls.Add(this.tasksGridControl);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 170);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(107, 255, 864, 732);
            this.dataLayoutControl1.OptionsView.UseParentAutoScaleFactor = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1333, 563);
            this.dataLayoutControl1.TabIndex = 27;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // tileControl1
            // 
            this.tileControl1.AllowDrag = false;
            this.tileControl1.AllowGlyphSkinning = true;
            this.tileControl1.AllowSelectedItem = true;
            this.tileControl1.AppearanceItem.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(114)))), ((int)(((byte)(191)))));
            this.tileControl1.AppearanceItem.Hovered.Options.UseBackColor = true;
            this.tileControl1.AppearanceItem.Normal.BackColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.tileControl1.AppearanceItem.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.tileControl1.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileControl1.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileControl1.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(69)))), ((int)(((byte)(148)))));
            this.tileControl1.AppearanceItem.Pressed.Options.UseBackColor = true;
            this.tileControl1.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(81)))), ((int)(((byte)(165)))));
            this.tileControl1.AppearanceItem.Selected.BorderColor = System.Drawing.Color.Transparent;
            this.tileControl1.AppearanceItem.Selected.ForeColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileControl1.AppearanceItem.Selected.Options.UseBorderColor = true;
            this.tileControl1.AppearanceItem.Selected.Options.UseForeColor = true;
            this.tileControl1.DragSize = new System.Drawing.Size(0, 0);
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.tileControl1.IndentBetweenItems = 10;
            this.tileControl1.ItemPadding = new System.Windows.Forms.Padding(7, 7, 7, 4);
            this.tileControl1.ItemSize = 90;
            this.tileControl1.Location = new System.Drawing.Point(40, 43);
            this.tileControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tileControl1.MaxId = 7;
            this.tileControl1.MaximumSize = new System.Drawing.Size(204, 0);
            this.tileControl1.MinimumSize = new System.Drawing.Size(204, 0);
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tileControl1.Padding = new System.Windows.Forms.Padding(0);
            this.tileControl1.SelectedItem = this.tileItemAll;
            this.tileControl1.Size = new System.Drawing.Size(204, 518);
            this.tileControl1.TabIndex = 26;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top;
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.tileItemAll);
            this.tileGroup2.Items.Add(this.tileItemInProgress);
            this.tileGroup2.Items.Add(this.tileItemNotStartedTask);
            this.tileGroup2.Items.Add(this.tileItemCompleted);
            this.tileGroup2.Name = "tileGroup2";
            this.tileGroup2.Text = null;
            // 
            // tileItemAll
            // 
            tileItemElement12.Appearance.Normal.FontSizeDelta = 128;
            tileItemElement12.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            tileItemElement12.Appearance.Normal.Options.UseFont = true;
            tileItemElement12.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement12.Appearance.Selected.FontSizeDelta = 128;
            tileItemElement12.Appearance.Selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(168)))), ((int)(((byte)(209)))));
            tileItemElement12.Appearance.Selected.Options.UseFont = true;
            tileItemElement12.Appearance.Selected.Options.UseForeColor = true;
            tileItemElement12.Image = global::DevExpress.SovFoods.Properties.Resources.AllTasks;
            tileItemElement12.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement12.Text = "183";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement12.TextLocation = new System.Drawing.Point(-2, -12);
            tileItemElement13.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement13.Text = "All Pallets";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.tileItemAll.Elements.Add(tileItemElement12);
            this.tileItemAll.Elements.Add(tileItemElement13);
            this.tileItemAll.Id = 0;
            this.tileItemAll.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItemAll.Name = "tileItemAll";
            this.tileItemAll.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItemAll_ItemClick);
            // 
            // tileItemInProgress
            // 
            tileItemElement14.Appearance.Normal.FontSizeDelta = 128;
            tileItemElement14.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            tileItemElement14.Appearance.Normal.Options.UseFont = true;
            tileItemElement14.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement14.Appearance.Selected.FontSizeDelta = 128;
            tileItemElement14.Appearance.Selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(168)))), ((int)(((byte)(209)))));
            tileItemElement14.Appearance.Selected.Options.UseFont = true;
            tileItemElement14.Appearance.Selected.Options.UseForeColor = true;
            tileItemElement14.Image = global::DevExpress.SovFoods.Properties.Resources.Completed;
            tileItemElement14.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement14.Text = "5";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement14.TextLocation = new System.Drawing.Point(-2, -12);
            tileItemElement15.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement15.Text = "Confirmed Pallets";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.tileItemInProgress.Elements.Add(tileItemElement14);
            this.tileItemInProgress.Elements.Add(tileItemElement15);
            this.tileItemInProgress.Id = 1;
            this.tileItemInProgress.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItemInProgress.Name = "tileItemInProgress";
            this.tileItemInProgress.Tag = "Status == \'InProgress\'";
            this.tileItemInProgress.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItemInProgress_ItemClick);
            // 
            // tileItemNotStartedTask
            // 
            tileItemElement16.Appearance.Normal.FontSizeDelta = 128;
            tileItemElement16.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            tileItemElement16.Appearance.Normal.Options.UseFont = true;
            tileItemElement16.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement16.Appearance.Selected.FontSizeDelta = 128;
            tileItemElement16.Appearance.Selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(168)))), ((int)(((byte)(209)))));
            tileItemElement16.Appearance.Selected.Options.UseFont = true;
            tileItemElement16.Appearance.Selected.Options.UseForeColor = true;
            tileItemElement16.Image = global::DevExpress.SovFoods.Properties.Resources.Urgent;
            tileItemElement16.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement16.Text = "3";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement16.TextLocation = new System.Drawing.Point(-2, -12);
            tileItemElement17.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement17.Text = "Removed Pallets";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.tileItemNotStartedTask.Elements.Add(tileItemElement16);
            this.tileItemNotStartedTask.Elements.Add(tileItemElement17);
            this.tileItemNotStartedTask.Id = 2;
            this.tileItemNotStartedTask.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItemNotStartedTask.Name = "tileItemNotStartedTask";
            this.tileItemNotStartedTask.Tag = "Status == \'NotStarted\'";
            this.tileItemNotStartedTask.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItemNotStartedTask_ItemClick);
            // 
            // tileItemCompleted
            // 
            tileItemElement18.Appearance.Normal.FontSizeDelta = 128;
            tileItemElement18.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(171)))));
            tileItemElement18.Appearance.Normal.Options.UseFont = true;
            tileItemElement18.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement18.Appearance.Selected.FontSizeDelta = 128;
            tileItemElement18.Appearance.Selected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(168)))), ((int)(((byte)(209)))));
            tileItemElement18.Appearance.Selected.Options.UseFont = true;
            tileItemElement18.Appearance.Selected.Options.UseForeColor = true;
            tileItemElement18.Image = global::DevExpress.SovFoods.Properties.Resources.Deferred;
            tileItemElement18.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement18.Text = "5";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement18.TextLocation = new System.Drawing.Point(-2, -12);
            tileItemElement19.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            tileItemElement19.Text = "Inactive Pallets";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.tileItemCompleted.Elements.Add(tileItemElement18);
            this.tileItemCompleted.Elements.Add(tileItemElement19);
            this.tileItemCompleted.Id = 4;
            this.tileItemCompleted.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItemCompleted.Name = "tileItemCompleted";
            this.tileItemCompleted.Tag = "Status == \'Completed\'";
            this.tileItemCompleted.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItemCompleted_ItemClick);
            // 
            // tasksGridControl
            // 
            this.tasksGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tasksGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tasksGridControl.Location = new System.Drawing.Point(257, 43);
            this.tasksGridControl.MainView = this.tasksGridView;
            this.tasksGridControl.Margin = new System.Windows.Forms.Padding(3, 3, 3, 200);
            this.tasksGridControl.Name = "tasksGridControl";
            this.tasksGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemComboBox1});
            this.tasksGridControl.Size = new System.Drawing.Size(1036, 520);
            this.tasksGridControl.TabIndex = 1;
            this.tasksGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tasksGridView});
            // 
            // tasksGridView
            // 
            this.tasksGridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.tasksGridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.tasksGridView.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.tasksGridView.Appearance.GroupFooter.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.tasksGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.tasksGridView.FooterPanelHeight = 60;
            this.tasksGridView.GridControl = this.tasksGridControl;
            this.tasksGridView.Name = "tasksGridView";
            this.tasksGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.tasksGridView.OptionsFind.AllowFindPanel = false;
            this.tasksGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.tasksGridView.OptionsView.ShowFooter = true;
            this.tasksGridView.OptionsView.ShowGroupPanel = false;
            this.tasksGridView.OptionsView.ShowIndicator = false;
            this.tasksGridView.RowHeight = 10;
            this.tasksGridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.tasksGridView_ShowingEditor);
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ProgressPadding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.repositoryItemProgressBar1.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.repositoryItemProgressBar1.ShowTitle = true;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemComboBox1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.PopupSizeable = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.tileControlLCI,
            this.buttonHide,
            this.tasksSLI});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1333, 563);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tasksGridControl;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(217, 43);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(1036, 520);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // tileControlLCI
            // 
            this.tileControlLCI.Control = this.tileControl1;
            this.tileControlLCI.CustomizationFormText = "tileControlLCI";
            this.tileControlLCI.Location = new System.Drawing.Point(0, 43);
            this.tileControlLCI.Name = "tileControlLCI";
            this.tileControlLCI.OptionsPrint.AllowPrint = false;
            this.tileControlLCI.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 3, 0, 2);
            this.tileControlLCI.Size = new System.Drawing.Size(207, 520);
            this.tileControlLCI.TextSize = new System.Drawing.Size(0, 0);
            this.tileControlLCI.TextVisible = false;
            // 
            // buttonHide
            // 
            this.buttonHide.AllowHotTrack = false;
            this.buttonHide.CustomizationFormText = " ";
            this.buttonHide.Location = new System.Drawing.Point(207, 43);
            this.buttonHide.MinSize = new System.Drawing.Size(10, 24);
            this.buttonHide.Name = "buttonHide";
            this.buttonHide.Size = new System.Drawing.Size(10, 520);
            this.buttonHide.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.buttonHide.Text = " ";
            this.buttonHide.TextSize = new System.Drawing.Size(53, 20);
            // 
            // tasksSLI
            // 
            this.tasksSLI.AllowHotTrack = false;
            this.tasksSLI.AllowHtmlStringInCaption = true;
            this.tasksSLI.AppearanceItemCaption.FontSizeDelta = 3;
            this.tasksSLI.AppearanceItemCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(82)))), ((int)(((byte)(163)))));
            this.tasksSLI.AppearanceItemCaption.Options.UseFont = true;
            this.tasksSLI.AppearanceItemCaption.Options.UseForeColor = true;
            this.tasksSLI.CustomizationFormText = "TASKS";
            this.tasksSLI.Location = new System.Drawing.Point(0, 0);
            this.tasksSLI.Name = "tasksSLI";
            this.tasksSLI.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 9, 9);
            this.tasksSLI.Size = new System.Drawing.Size(1253, 43);
            this.tasksSLI.Text = "TASKS";
            this.tasksSLI.TextSize = new System.Drawing.Size(53, 25);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.layoutControl1.Controls.Add(this.windowsUIButtonPanel1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.layoutControl1.Location = new System.Drawing.Point(0, 653);
            this.layoutControl1.MinimumSize = new System.Drawing.Size(0, 80);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(350, 337, 1203, 653);
            this.layoutControl1.OptionsView.AlwaysScrollActiveControlIntoView = false;
            this.layoutControl1.OptionsView.ControlDefaultMaxSizeCalcMode = DevExpress.XtraLayout.ControlMaxSizeCalcMode.UseControlMaximumSize;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(1333, 80);
            this.layoutControl1.TabIndex = 32;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // windowsUIButtonPanel1
            // 
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseFont = true;
            this.windowsUIButtonPanel1.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Normal.FontSizeDelta = -1;
            this.windowsUIButtonPanel1.AppearanceButton.Normal.Options.UseFont = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseFont = true;
            this.windowsUIButtonPanel1.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.windowsUIButtonPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Enable/Disable", ((System.Drawing.Image)(resources.GetObject("windowsUIButtonPanel1.Buttons")))),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Print", ((System.Drawing.Image)(resources.GetObject("windowsUIButtonPanel1.Buttons1"))), -1, DevExpress.XtraBars.Docking2010.ImageLocation.Default, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", true, -1, true, null, true, false, true, null, null, -1, false, true)});
            this.windowsUIButtonPanel1.ForeColor = System.Drawing.Color.White;
            this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 10);
            this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.windowsUIButtonPanel1.Size = new System.Drawing.Size(1329, 59);
            this.windowsUIButtonPanel1.TabIndex = 5;
            this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanel1.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanel1_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1333, 80);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.windowsUIButtonPanel1;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem1";
            this.layoutControlItem2.Size = new System.Drawing.Size(1333, 80);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.TrimClientAreaToControl = false;
            // 
            // icBackground
            // 
            this.icBackground.ImageSize = new System.Drawing.Size(50, 50);
            this.icBackground.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icBackground.ImageStream")));
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1333, 733);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.flyoutPanel1);
            this.Controls.Add(this.modulesContainer);
            this.Controls.Add(this.mainTileBar);
            this.Controls.Add(this.tileNavPane);
            this.Controls.Add(this.customTileBarDropDownContainter);
            this.Controls.Add(this.productsTileBarDropDownContainter);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pack To Pallete";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.productsTileBarDropDownContainter)).EndInit();
            this.productsTileBarDropDownContainter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.customTileBarDropDownContainter)).EndInit();
            this.customTileBarDropDownContainter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flyoutPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tasksGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileControlLCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksSLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icBackground)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private XtraBars.Navigation.TileNavPane tileNavPane;
        private XtraBars.Navigation.NavButton navButtonHelp;
        private XtraBars.Navigation.NavButton navButtonClose;
        public XtraBars.Navigation.TileBar mainTileBar;
        private DevExpress.XtraEditors.XtraUserControl modulesContainer;
        private TransitionManager transitionManager1;
        private XtraBars.Navigation.TileBarItem dashboardTileBarItem;
        private XtraBars.Navigation.TileBarItem applicationSettingsTileBarItem;
        private XtraBars.Navigation.TileBarItem ipcTileBarItem;
        private XtraBars.Navigation.TileBarGroup tileBarGroup3;
        private XtraBars.Navigation.TileBarItem reportsTileBarItem;
        private XtraBars.Navigation.TileBarDropDownContainer customTileBarDropDownContainter;
        private XtraBars.Navigation.TileBarDropDownContainer productsTileBarDropDownContainter;
        private XtraBars.Navigation.TileBarGroup tileBarGroup4;
        private XtraBars.Navigation.TileBarItem customMyAccountTileBarItem;
        private XtraBars.Navigation.TileBarItem customJohnAccountTileBarItem;
        private XtraBars.Navigation.TileBarItem customTopStoresTileBarItem;
        public XtraBars.Navigation.TileBar productTileBar;
        private XtraBars.Navigation.TileBarGroup tileBarGroup1;
        private XtraBars.Navigation.TileBarItem StalePalletsReport;
        private XtraBars.Navigation.TileBarItem exeptionReport;
        private XtraBars.Navigation.TileBarItem fullReport;
        private XtraBars.Navigation.NavButton navButtonHome;
        private Utils.FlyoutPanel flyoutPanel1;
        private XtraBars.Navigation.TileBarItem AllStoresTBI;
        public XtraBars.Navigation.TileBar customTileBar;
        private XtraBars.BarDockControl barDockControlLeft;
        private XtraBars.BarDockControl barDockControlRight;
        private XtraBars.BarDockControl barDockControlBottom;
        private XtraBars.BarDockControl barDockControlTop;
        private XtraBars.BarManager barManager1;
        private XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private XtraEditors.TileControl tileControl1;
        private XtraEditors.TileGroup tileGroup2;
        private XtraEditors.TileItem tileItemAll;
        private XtraEditors.TileItem tileItemInProgress;
        private XtraEditors.TileItem tileItemNotStartedTask;
        private XtraEditors.TileItem tileItemCompleted;
        private XtraGrid.GridControl tasksGridControl;
        private XtraGrid.Views.Grid.GridView tasksGridView;
       // private XtraGrid.Columns.GridColumn Priority;
        private XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemComboBox1;
        private XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraLayout.LayoutControlItem layoutControlItem1;
        private XtraLayout.LayoutControlItem tileControlLCI;
        private XtraLayout.SimpleLabelItem buttonHide;
        private XtraLayout.SimpleLabelItem tasksSLI;
        public XtraLayout.LayoutControl layoutControl1;
        private XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
        private XtraLayout.LayoutControlGroup layoutControlGroup2;
        private XtraLayout.LayoutControlItem layoutControlItem2;
        private Utils.ImageCollection icBackground;
        //private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}
