﻿using System;
using System.IO;
using System.Windows.Forms;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Navigation;
using System.Drawing;
using DevExpress.Utils.Gesture;
using DevExpress.Utils.TouchHelpers;
using DevExpress.Utils.Animation;
using DevExpress.XtraBars.Docking2010.Customization;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Taskbar.Core;
using DevExpress.Utils.Taskbar;
using DevExpress.SovFoods.Modules;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Data;
using DevExpress.XtraGrid.Views.Tile;
using DevExpress.XtraGrid;
using DevExpress.Data.Filtering;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Configuration;


namespace DevExpress.SovFoods {
    public partial class Displays : XtraForm {
    
        public Assembly myModuleAssembly;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public Displays()
        {
            TaskbarHelper.InitDemoJumpList(TaskbarAssistant.Default, this);
            ShowSplashScreen();
            InitializeComponent();
            DevExpress.Utils.About.UAlgo.Default.DoEventObject(DevExpress.Utils.About.UAlgo.kDemo, DevExpress.Utils.About.UAlgo.pWinForms, this);
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseDefaultWaitForm();
        }

        void ShowSplashScreen() {
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowDefaultWaitForm();
        }

        void navButtonClose_ElementClick(object sender, NavElementEventArgs e) {
            this.Hide();
            Form st = new Displays();
            Form main = new MainForm();

            main.Show();
            st.Close();
        }

        //Insert new ipc to the database
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (var sovDB = new PackToPalleteEntities())
                {
                    var displays = sovDB.Set<IPCDisplay>();
                    displays.Add(new IPCDisplay { DisplayName = txtDisplayName.Text, Description = txtDescription.Text, IPAddress = txtDisplayIpAddress.Text});
                    sovDB.SaveChanges();

                    this.Hide();
                    Form st = new Displays();
                    Form main = new MainForm();

                    main.Show();
                    st.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
